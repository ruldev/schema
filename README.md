# RUcore Metadata XSD #

This utility provides the ability to validate exported RUcore metadata.

### What is this repository for? ###

* Validating RUcore metadata
* Integrating into any RUcore resource export to ensure the integrity of the metadata stored in RUcore

### What are the parts of this repository? ###

* MODS descriptive metadata - general MODS XSD
* RUL Descriptive metadata - RUL descriptive metadata extension XSD
* RUL Rights metadata - RUL rights metadata
* RUL Source metadata - RUL source metadata
* RUL Technical metadata - RUL technical metadata




